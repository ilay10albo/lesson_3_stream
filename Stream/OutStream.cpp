#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	this->_file = stdout; // Default stream is STDOUT
}

OutStream::~OutStream()
{
	if(this->_file)
	{
		fclose(_file); // Close file stream if exist
	}
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_file,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_file,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE*))
{
	pf(_file);
	return *this;
}


void endline(FILE* _file)
{
	fprintf(_file,"\n"); // Write new line to current stream
}
