#include "OutStreamEncrypted.h"


OutStreamEncrypted::OutStreamEncrypted(int key)
{
	this->_key = key; // Get string 
}
OutStreamEncrypted ::~OutStreamEncrypted()
{

}
OutStreamEncrypted &OutStreamEncrypted::operator<<(const char *str)
{	
	char * copy = new char[strlen(str) - 1 ];
	strcpy(copy, str); // Becuase constant 
	copy = this->Encrypt(copy); // Get encrypted string 
	fprintf(this->_file, "%s", copy);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	
	char* new_st  = new char[10];
	sprintf(new_st, "%d", num); // Convert number to string 
	new_st = Encrypt(new_st); // Send key 
	fprintf(this->_file, new_st); // Print encrypted message to screen
	delete[] new_st;
	return *this;

}
OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE*))
{
	pf(_file);
	return *this;
}

int OutStreamEncrypted::getSizeOfNumber(int number)
{
	int counter = 0;
	while (number!=0)
	{
		counter++;
		number /= 10;
	}return counter;
}

char * OutStreamEncrypted :: Encrypt(char* str)
{
	char* CryptStr = str;
	for (int Current = 0; Current < strlen(str); Current++) 
		CryptStr[Current] += this->_key; // Forward value
	return CryptStr;
}