#include "Logger.h"
Logger::Logger() : OutStream()
{
	_startLine = true;
}
Logger::~Logger()
{

}

void Logger::setStartLine()
{
	static unsigned int log_counter = 0;
	log_counter++; // Forward counter
	if (_startLine) // If new line print number of log msg
	{
		os  << log_counter << ".";
	}
	if (!this->_startLine) // If not new line 
	{
		this->_startLine = true; 
		log_counter++;
	}
	
}
Logger& operator<<(Logger& l, const char *msg)
{
	if (l._startLine)
	{
		l.setStartLine();
		l.os  <<"LOG " << msg << " ";
		l._startLine = false;  // Reset flag
	}
	else // Print normal to string 
	{
		l.os << msg << " ";
	}
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	if (l._startLine)
	{
		l.setStartLine();
		l.os << "LOG " << num << " ";
		l._startLine = false; // Reset flag 
	}
	else // Print normal to stream
	{
		l.os << num << " ";
	}
	return l;
}


Logger& operator<<(Logger& l, void(*pf)(FILE*))
{
	l._startLine = true; // Reset line mode	
	pf(l._file);
	return l;
}