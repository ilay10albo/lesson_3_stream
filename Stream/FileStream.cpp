#include "FileStream.h"

FileStream::~FileStream() 
{
	if (this->_file) // If file stream is valid
	{
		fclose(this->_file); // Close file
		this->_file = nullptr;
	}
}

FileStream::FileStream(const char *str)
{
	this->_file = fopen(str, "w"); // Make new fle 
}

