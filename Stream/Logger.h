#pragma once
#include "OutStream.h"
#include <stdio.h>


class Logger : public OutStream
{
	static unsigned int log_counter; // Static log counter
	OutStream os;
	bool _startLine;  
	void setStartLine(); // Function decides whether to print a new line or not

public:
	Logger();
	~Logger();
	friend Logger& operator<<(Logger& l, const char *msg); // Writes string to logger stream
	friend Logger& operator<<(Logger& l, int num); // Writes number to logger stream 
	friend Logger& operator<<(Logger& l, void(*pf)(FILE*)); // Gets a pointer to function within logger stream
};

