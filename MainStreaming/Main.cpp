#pragma comment(lib, "Stream.lib")
#include "FileStream.h"
#include "Logger.h"
#include "OutStream.h"
#include "OutStreamEncrypted.h"
#include <stdio.h>
int main()
{
	Logger a;
	OutStream b;
	OutStreamEncrypted c(4); // Must give it an inital key to encrypt
	FileStream d("File.txt");
	a << "This is a log message " << 1 << endline;
	a << "This is another log " << 1 << endline;
	b << "Normal stdout stream test " << 1 << endline;
	c << "Enqrypted script test" << 11 << endline;
	d << "Writing to file test " << 11 << endline; // Please check file

	getchar();
}