#pragma once
#include <string>
#pragma warning (disable:4996)

class OutStream 
{
protected:
	FILE * _file;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str); // Writes string to current stream
	OutStream& operator<<(int num); // Writes number to current stream
	OutStream& operator<<(void(*pf)(FILE*)); // Gets a pointer and calls to that function
};

void endline(FILE* file_add_stream); // New line ..