#pragma once
#include "OutStream.h"
#include <iostream>
#include <string>								
#pragma warning (disable:4996)					
												
												
class OutStreamEncrypted : OutStream			
{
private:
	int _key;
public:
	char * Encrypt(char* str); // Encrypts the string 
	int getSizeOfNumber(int number); // Gets number size
	OutStreamEncrypted& operator<<(const char *str); // Writes string to current stream
	OutStreamEncrypted& operator<<(int num); // Writes number to current stream
	OutStreamEncrypted& operator<<(void(*pf)(FILE*)); // Gets a pointer and calls to that function
	OutStreamEncrypted(int key);
	~OutStreamEncrypted();
};
